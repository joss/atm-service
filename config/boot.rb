require 'rubygems'
require 'bundler/setup'

Bundler.require :default, ENV['RACK_ENV']

Dir[File.expand_path('../../config/initializers/*.rb', __FILE__)].each do |initializer|
  require initializer
end

Dir[File.expand_path('../../lib/errors/*.rb', __FILE__)].each do |error|
  require error
end

Dir[File.expand_path('../../lib/models/*.rb', __FILE__)].each do |model|
  require model
end

Dir[File.expand_path('../../lib/services/*.rb', __FILE__)].each do |service|
  require service
end

Dir[File.expand_path('../../lib/validators/*.rb', __FILE__)].each do |validator|
  require validator
end

Dir[File.expand_path('../../api/endpoints/*.rb', __FILE__)].each do |endpoint|
  require endpoint
end
