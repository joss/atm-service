module AtmService
  class Withdraw
    def initialize(amount)
      @amount = amount
    end

    def self.call(*args)
      new(*args).call
    end

    def call
      raise ::AtmService::Error::FundsNotEnough if @amount > AtmCash.balance

      cash = AtmService::GreedyCashFinder.call(AtmCash.available_notes, @amount)
      raise ::AtmService::Error::NotesNotEnough unless cash

      cash.each do |den, count|
        AtmCash.withdraw(den, count)
      end

      cash
    end
  end
end
