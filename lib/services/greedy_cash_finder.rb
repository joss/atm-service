module AtmService
  class GreedyCashFinder
    MAX_ITERATIONS = 100

    def initialize(notes, amount)
      @notes = notes
      @amount = amount

      @current = {}
      @reductions = {}
      @reductions_current = nil
    end

    def self.call(*args)
      new(*args).call
    end

    def call
      iterations = 0
      rest = @amount

      loop do
        @notes.sort.reverse.each do |denomination, count|
          current_rest = update_rest(denomination, rest, count)

          next if current_rest == -1
          rest = current_rest

          return @current if rest == 0
        end

        fill_reductions

        @current = {}
        rest = @amount

        iterations += 1
        break if iterations > MAX_ITERATIONS
      end
    end

    private

    def update_rest(den, amnt, available_den)
      quotient = amnt / den

      return -1 if quotient == 0

      remainder = amnt % den
      @current[den] = [quotient - @reductions[den].to_i, available_den].min

      amnt - (@current[den] * den)
    end

    def fill_reductions
      note_dens = @current.keys.sort.reverse
      reductions_current_idx = note_dens.index(@reductions_current) || -1
      reductions_next = note_dens[reductions_current_idx + 1] || note_dens.first
      @reductions_current = reductions_next

      @reductions[reductions_next] ||= 0
      @reductions[reductions_next] += 1
    end
  end
end
