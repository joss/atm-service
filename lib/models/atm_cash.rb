class AtmCash
  include Redis::Objects

  NOMINALS = [1, 2, 5, 10, 25, 50].freeze

  counter :note_1, global: true
  counter :note_2, global: true
  counter :note_5, global: true
  counter :note_10, global: true
  counter :note_25, global: true
  counter :note_50, global: true

  def self.available_notes
    notes_with_count = NOMINALS.map do |v|
      count = AtmCash.count_of(v)
      next [] if count == 0

      [v, count]
    end

    Hash[*notes_with_count.flatten]
  end

  def self.balance
    NOMINALS.inject(0) { |sum, n| sum += n * AtmCash.count_of(n) }
  end

  def self.count_of(nominal)
    AtmCash.get_counter(:"note_#{nominal}")
  end

  def self.deposit(nominal, count)
    AtmCash.increment_counter(:"note_#{nominal}", nil, count)
  end

  def self.withdraw(nominal, count)
    AtmCash.decrement_counter(:"note_#{nominal}", nil, count) do |val|
      raise AtmService::Error::NotesNotEnough, nominal if val.negative?
      val
    end
  end
end
