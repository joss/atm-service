module AtmService
  module Error
    class NotesNotEnough < StandardError
      def initialize(nominal = nil)
        @nominal = nominal
      end

      def message
        unless @nominal
          return 'ATM does not have enough notes. ' \
            "Please use amount multiple to #{AtmCash.available_notes.keys.join(', ')}"
        end

        "ATM does not have enough notes with nominal #{@nominal}"
      end
    end
  end
end
