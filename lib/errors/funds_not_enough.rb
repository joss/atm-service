module AtmService
  module Error
    class FundsNotEnough < StandardError
      def message
        'ATM does not have enough money'
      end
    end
  end
end
