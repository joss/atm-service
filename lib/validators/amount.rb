class Amount < Grape::Validations::Base
  def validate_param!(attr_name, params)
    if params[attr_name] && @option[:min] && params[attr_name] < @option[:min]
      fail Grape::Exceptions::Validation,
        params: [@scope.full_name(attr_name)],
        message: "must not be less than #{@option[:min]}"
    end
  end
end
