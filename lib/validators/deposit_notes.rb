class DepositNotes < Grape::Validations::Base
  def validate_param!(attr_name, params)
    notes = params[attr_name]

    return unless notes
    return unless notes.is_a?(Hash)

    if (notes.keys.map(&:to_i) - AtmCash::NOMINALS).any?
      fail Grape::Exceptions::Validation,
        params: ['ATM'],
        message: "accepts only #{AtmCash::NOMINALS.join(', ')} denominations"
    end

    if notes.values.any? { |v| v.to_i < 0 }
      fail Grape::Exceptions::Validation,
        params: ['ATM'],
        message: 'denominations count should be positive'
    end
  end
end
