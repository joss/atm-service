require 'spec_helper'

RSpec.describe Api::Endpoints::User, type: :request do
  let(:deposit_with_all_nominals) do
    AtmCash::NOMINALS.each do |nominal|
      AtmCash.deposit(nominal, rand(20..50))
    end
  end

  context 'withdraw' do
    it 'returns error without amount parameter' do
      post '/v1/user/withdraw'

      expect(last_response.status).to eq(400)
      expect(JSON.parse(last_response.body).keys).to include('error')
      expect(JSON.parse(last_response.body)['error']).to eq('amount is missing')
    end

    it 'returns error when balance is low' do
      post '/v1/user/withdraw', amount: 200

      expect(last_response.status).to eq(422)
      expect(JSON.parse(last_response.body).keys).to include('error')
      expect(JSON.parse(last_response.body)['error']).to eq('ATM does not have enough money')
    end

    it 'returns error when cash can not be prepared' do
      AtmCash.deposit(5, 10)
      AtmCash.deposit(50, 10)
      AtmCash.deposit(25, 10)

      post '/v1/user/withdraw', amount: 27

      expect(last_response.status).to eq(422)
      expect(JSON.parse(last_response.body).keys).to include('error')
      expect(JSON.parse(last_response.body)['error'])
        .to eq('ATM does not have enough notes. Please use amount multiple to 5, 25, 50')
    end

    it 'returns error when amount is less than 1' do
      AtmCash.deposit(1, 100)

      post '/v1/user/withdraw', amount: 0

      expect(last_response.status).to eq(400)
      expect(JSON.parse(last_response.body).keys).to include('error')
      expect(JSON.parse(last_response.body)['error']).to eq('amount must not be less than 1')
    end

    it 'returns notes' do
      deposit_with_all_nominals

      post '/v1/user/withdraw', amount: 200

      expect(last_response.status).to eq(201)
      expect(JSON.parse(last_response.body).keys).to include('notes')
      expect(JSON.parse(last_response.body)['notes']).to be_kind_of(Hash)
    end
  end
end
