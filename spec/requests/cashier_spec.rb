require 'spec_helper'

RSpec.describe Api::Endpoints::Cashier, type: :request do
  context 'deposit' do
    let(:success_request) { post '/v1/cashier/deposit', notes: { 25 => 30, 50 => 20 } }

    it 'returns error without notes parameter' do
      post '/v1/cashier/deposit'

      expect(last_response.status).to eq(400)
      expect(JSON.parse(last_response.body).keys).to include('error')
      expect(JSON.parse(last_response.body)['error']).to eq('notes is missing')
    end

    it 'returns error when passed something other than Hash' do
      [10, '10', []].each do |wrong_value|
        post '/v1/cashier/deposit', notes: wrong_value

        expect(last_response.status).to eq(400)
        expect(JSON.parse(last_response.body).keys).to include('error')
        expect(JSON.parse(last_response.body)['error']).to eq('notes is invalid')
      end
    end

    it 'returns notes' do
      success_request

      expect(last_response.status).to eq(201)
      expect(JSON.parse(last_response.body).keys).to include('notes')
      expect(JSON.parse(last_response.body)['notes']).to be_kind_of(Hash)
      expect(JSON.parse(last_response.body)['notes']).to eq('25' => 30, '50' => 20)
    end

    it 'returns error when notes contains wrong denominations' do
      post '/v1/cashier/deposit', notes: { 10 => 11, 12 => 13 }

      expect(last_response.status).to eq(400)
      expect(JSON.parse(last_response.body).keys).to include('error')
      expect(JSON.parse(last_response.body)['error']).to eq('ATM accepts only 1, 2, 5, 10, 25, 50 denominations')
    end

    it 'returns error when notes contains wrong denominations' do
      post '/v1/cashier/deposit', notes: { 10 => -11 }

      expect(last_response.status).to eq(400)
      expect(JSON.parse(last_response.body).keys).to include('error')
      expect(JSON.parse(last_response.body)['error']).to eq('ATM denominations count should be positive')
    end

    it 'increases cash balance' do
      expect { success_request }
        .to change{ AtmCash.count_of(50) }.from(0).to(20)
        .and change{ AtmCash.count_of(25) }.from(0).to(30)
        .and change{ AtmCash.count_of(1) }.by(0)
        .and change{ AtmCash.count_of(10) }.by(0)
    end

    it 'updates existing note count' do
      AtmCash.deposit(25, 11)
      AtmCash.deposit(50, 23)
      AtmCash.deposit(2, 15)

      expect { success_request }
        .to change{ AtmCash.count_of(50) }.from(23).to(43)
        .and change{ AtmCash.count_of(25) }.from(11).to(41)
        .and change{ AtmCash.count_of(2) }.by(0)
        .and change{ AtmCash.count_of(5) }.by(0)
    end
  end
end
