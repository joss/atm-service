require 'spec_helper'

RSpec.describe AtmCash do
  let(:deposit_with_all_nominals) do
    AtmCash::NOMINALS.each do |nominal|
      AtmCash.deposit(nominal, rand(20..50))
    end
  end

  context 'deposit' do
    it 'updates note-1 count' do
      expect do
        AtmCash.deposit(1, 12)
      end.to change { AtmCash.count_of(1) }.from(0).to(12)
    end

    it 'updates note-2 count' do
      expect do
        AtmCash.deposit(2, 12)
      end.to change { AtmCash.count_of(2) }.from(0).to(12)
    end

    it 'updates note-5 count' do
      expect do
        AtmCash.deposit(5, 12)
      end.to change { AtmCash.count_of(5) }.from(0).to(12)
    end

    it 'updates note-10 count' do
      expect do
        AtmCash.deposit(10, 12)
      end.to change { AtmCash.count_of(10) }.from(0).to(12)
    end

    it 'updates note-25 count' do
      expect do
        AtmCash.deposit(25, 12)
      end.to change { AtmCash.count_of(25) }.from(0).to(12)
    end

    it 'updates note-50 count' do
      expect do
        AtmCash.deposit(50, 12)
      end.to change { AtmCash.count_of(50) }.from(0).to(12)
    end

    it 'can be applied many times' do
      expect do
        AtmCash.deposit(50, 12)
        AtmCash.deposit(50, 23)
      end.to change { AtmCash.count_of(50) }.from(0).to(35)
    end
  end

  context 'withdraw' do
    before(:each) { deposit_with_all_nominals }

    it 'updates note-1 count' do
      expect do
        AtmCash.withdraw(1, 12)
      end.to change { AtmCash.count_of(1) }.by(-12)
    end

    it 'updates note-2 count' do
      expect do
        AtmCash.withdraw(2, 12)
      end.to change { AtmCash.count_of(2) }.by(-12)
    end

    it 'updates note-5 count' do
      expect do
        AtmCash.withdraw(5, 12)
      end.to change { AtmCash.count_of(5) }.by(-12)
    end

    it 'updates note-10 count' do
      expect do
        AtmCash.withdraw(10, 12)
      end.to change { AtmCash.count_of(10) }.by(-12)
    end

    it 'updates note-25 count' do
      expect do
        AtmCash.withdraw(25, 12)
      end.to change { AtmCash.count_of(25) }.by(-12)
    end

    it 'updates note-50 count' do
      expect do
        AtmCash.withdraw(50, 12)
      end.to change { AtmCash.count_of(50) }.by(-12)
    end

    it 'can be applied many times' do
      expect do
        AtmCash.withdraw(50, 5)
        AtmCash.withdraw(50, 8)
      end.to change { AtmCash.count_of(50) }.by(-13)
    end

    it 'raises error when notes not enough' do
      expect { AtmCash.withdraw(50, 55) }
        .to raise_error(::AtmService::Error::NotesNotEnough, /with nominal 50/)
      expect { AtmCash.withdraw(50, 55) rescue '' }.to change { AtmCash.count_of(50) }.by(0)
    end
  end

  context 'available_notes' do
    it 'returns all nominals if available' do
      deposit_with_all_nominals

      expect(AtmCash.available_notes.keys).to eq(AtmCash::NOMINALS)
    end

    it 'skips notes with zero count' do
      AtmCash.deposit(50, 12)
      AtmCash.deposit(25, 22)

      expect(AtmCash.available_notes.keys).to eq([25, 50])
    end
  end

  context 'balance' do
    it 'is zero when empty' do
      expect(AtmCash.balance).to eq(0)
    end

    it 'is increased after deposit' do
      deposit_with_all_nominals

      expect { AtmCash.deposit(2, 10) }.to change(AtmCash, :balance).by(20)
    end

    it 'is decreased after withdraw' do
      deposit_with_all_nominals

      expect { AtmCash.withdraw(25, 5) }.to change(AtmCash, :balance).by(-125)
    end
  end
end
