require 'spec_helper'

RSpec.describe AtmService::GreedyCashFinder do
  let(:all_10) { { 50 => 10, 25 => 10, 10 => 10, 5 => 10, 2 => 10, 1 => 10 } }
  let(:empty_small_dens) { { 5 => 0, 2 => 0, 1 => 0 } }

  context 'service' do
    it 'returns nil if notes not enough' do
      expect(AtmService::GreedyCashFinder.call({}, 10)).to eq(nil)
    end

    it 'gives large denomination first' do
      expect(AtmService::GreedyCashFinder.call(all_10, 165))
        .to eq(50 => 3, 10 => 1, 5 => 1)
    end

    it 'does not exceeds available notes' do
      expect(AtmService::GreedyCashFinder.call(all_10.merge(50 => 2), 165))
        .to eq(50 => 2, 25 => 2, 10 => 1, 5 => 1)
    end

    it 'finds cash without small denominations' do
      expect(AtmService::GreedyCashFinder.call(all_10.merge(empty_small_dens), 165))
        .to eq(50 => 2, 25 => 1, 10 => 4)
    end

    it 'sorts notes itself' do
      example = { 1 => 34, 2 => 32, 5 => 50, 10 => 35, 25 => 37, 50 => 33 }

      expect(AtmService::GreedyCashFinder.call(example, 200)).to eq(50 => 4)
    end
  end
end
