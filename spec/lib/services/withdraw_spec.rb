require 'spec_helper'

RSpec.describe AtmService::Withdraw do
  let(:deposit_with_all_nominals) do
    AtmCash::NOMINALS.each do |nominal|
      AtmCash.deposit(nominal, rand(20..50))
    end
  end

  context 'with empty balance' do
    it 'raises error when balance is not enough' do
      expect { AtmService::Withdraw.call(200) }.to raise_error(::AtmService::Error::FundsNotEnough)
      expect { AtmService::Withdraw.call(200) rescue '' }.to change { AtmCash.balance }.by(0)
    end
  end

  context 'when ATM is full' do
    before(:each) { deposit_with_all_nominals }

    it 'decreases balance' do
      expect { AtmService::Withdraw.call(200) }.to change { AtmCash.balance }.by(-200)
    end
  end
end
