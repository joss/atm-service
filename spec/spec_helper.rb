ENV['RACK_ENV'] = 'test'

require 'rack/test'
require File.expand_path('../../config/environment', __FILE__)

RSpec.configure do |config|
  config.color = true
  config.formatter = :documentation

  config.mock_with :rspec
  config.expect_with :rspec

  config.raise_errors_for_deprecations!

  config.before(:each) { Redis::Objects.redis.flushdb }

  config.include Rack::Test::Methods, type: :request
end

def app
  Api::Base
end
