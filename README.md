Application is currently hosted on [Heroku](https://sh-atm-service.herokuapp.com)

## Basic commands
  - `curl -H 'Content-Type: application/json' -XPOST https://sh-atm-service.herokuapp.com/v1/cashier/deposit -d '{"notes":{"25": 10}}'`
  - `curl -H 'Content-Type: application/json' -XPOST https://sh-atm-service.herokuapp.com/v1/user/withdraw -d '{"amount": 50}'`

# TODO

### Grape
  - Use grape-entity
  - Version header and latest version by default
  - API documentation. [RSpec API Doc](https://github.com/zipmark/rspec_api_documentation) preferred

### Redis
  - ~~handle negative counter~~
  - [Use Redlock](https://github.com/leandromoreira/redlock-rb) for deposit and withdraw
  - [Atomic Rant](http://www.nateware.com/an-atomic-rant.html#.WpHfIemWaXI)

### Refactoring
  - AtmCash counters should be private. Or maybe use Hash instead

### Improvement
  - do not use iterations limit at GreedyCashFinder
  - normalize terminology
  - project structure and class/modules naming

### Validation
  - ~~deposit values are positive~~
  - ~~deposit values included in nominals constant~~
  - ~~deposit parameter is valid hash~~
  - ~~withdraw amount is positive~~

### Specs
  - More test cases for GreedyCashFinder
  - Test validations inside validation, not request specs
