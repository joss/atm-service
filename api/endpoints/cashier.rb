module Api
  module Endpoints
    class Cashier < Grape::API
      namespace :cashier do
        params do
          requires :notes, type: Hash, deposit_notes: true
        end
        post :deposit do
          params[:notes].each do |nominal, value|
            AtmCash.deposit(nominal, value)
          end

          { notes: AtmCash.available_notes }
        end
      end
    end
  end
end
