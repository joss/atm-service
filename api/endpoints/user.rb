module Api
  module Endpoints
    class User < Grape::API
      namespace :user do
        params do
          requires :amount, type: Integer, amount: { min: 1 }
        end
        post :withdraw do
          cash = AtmService::Withdraw.call(params[:amount])

          { notes: cash }
        end
      end
    end
  end
end
