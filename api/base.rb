module Api
  class Base < Grape::API
    if ENV['RACK_ENV'] == 'development'
      insert_after Grape::Middleware::Formatter, Grape::Middleware::Logger
    end

    rescue_from ::AtmService::Error::FundsNotEnough, ::AtmService::Error::NotesNotEnough do |e|
      error!(e.message, 422)
    end

    version 'v1', using: :path
    format :json

    mount Endpoints::User
    mount Endpoints::Cashier
  end
end
